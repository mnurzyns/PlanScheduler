#!/bin/bash

RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

cd $BUILD_PATH

cmake ..
if [ $? -ne 0 ]; then
    echo -e ${RED}CMake FAILED${NOCOLOR}
    exit
fi

make
if [ $? -ne 0 ]; then
    echo -e ${RED}make FAILED${NOCOLOR}
    exit
fi

#VALGRIND
make valgrind >/dev/null
VALGRIND_ERRORS_SUMARY=$(cat valgrindOutput.txt | grep "ERROR SUMMARY")
ERRORS_NUMBER_POSITION_IN_ERRORS_SUMARY=25
VALGRIND_ERRORS_NUMBER=${VALGRIND_ERRORS_SUMARY:$ERRORS_NUMBER_POSITION_IN_ERRORS_SUMARY:1}

if [ $VALGRIND_ERRORS_NUMBER -eq 0 ] ; then
	GREEN_LIST+=("Valgrind - ok")
else
	RED_LIST+=("Valgrind - NOT OK!")
	vim valgrindOutput.txt
fi

#UT_COVERAGE
lcov --quiet --capture --directory . --output-file coverage.info
lcov --quiet --remove coverage.info '*Test*' '/usr/include/*' '/usr/lib/*' '*googletest*' -o utCoverage.info
genhtml --quiet utCoverage.info --output-directory out
NUMBER_OFF_ALL_DIRECTORIES=$(grep -c coverPer out/index.html)
NUMBER_OFF_COVERAGED_DIRECTORIES=$(grep -c coverPer.*\100.0 out/index.html)

if [ $NUMBER_OFF_ALL_DIRECTORIES -eq $NUMBER_OFF_COVERAGED_DIRECTORIES ] ; then
	GREEN_LIST+=("UT coverage - ok")
else
	RED_LIST+=("UT coverage - NOT OK!")
	google-chrome out/index.html
fi

#CLANG-TIDY
python ../scripts/local_scripts/run-clang-tidy.py > clangTidyOutput.txt
CLANG_TIDY_WARNING_COUNT=`grep warning clangTidyOutput.txt | wc -l`

if [ $CLANG_TIDY_WARNING_COUNT -eq 0 ] ; then
	GREEN_LIST+=("Clang-tidy - ok")
else
	RED_LIST+=("Clang-tidy - NOT OK!")
	vim clangTidyOutput.txt
fi

#UT
./bin/plan_scheduler_UT > utOutput.txt
FAILED_TEST_COUNT=`grep 'FAILED TEST' utOutput.txt | wc -l`

if [ $FAILED_TEST_COUNT -eq 0 ] ; then
	GREEN_LIST+=("UT - ok")
else
	RED_LIST+=("UT - NOT OK!")
	vim utOutput.txt
fi

#CPPCHECK
cppcheck --enable=all --inconclusive --quiet --check-config -ibuild -iCMakeLists* -iLICENSE -iREADME.md -iExternal_GTest.cmake ../. 2> cppcheckOutput.txt
CPPCHECK_ERRORS=$(grep -E '(warning)|(style)|(performance)|(portability)|(unusedFunction)' cppcheckOutput.txt | wc -l)

if [ $CPPCHECK_ERRORS -eq 0 ] ; then
	GREEN_LIST+=("Cppcheck - ok")
else
	RED_LIST+=("Cppcheck - NOT OK!")
	vim cppcheckOutput.txt
fi

#SUMMARY
for i in "${RED_LIST[@]}"
do
	echo -e ${RED}$i${NOCOLOR}
done

for i in "${GREEN_LIST[@]}"
do
	echo -e ${GREEN}$i${NOCOLOR}
done

