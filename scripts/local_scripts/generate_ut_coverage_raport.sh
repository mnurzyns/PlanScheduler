#!/bin/bash

cd $BUILD_PATH
cmake ..
make
cd bin
./plan_scheduler_UT
cd ..
lcov --quiet --capture --directory . --output-file coverage.info
lcov --quiet --remove coverage.info '*Test*' '/usr/include/*' '/usr/lib/*' '*googletest*' -o utCoverage.info
genhtml --quiet utCoverage.info --output-directory out

if [ ! -z $1 ] ; then 
    $1 out/index.html &
fi
