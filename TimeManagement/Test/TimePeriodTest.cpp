#include "TimePeriodTest.hpp"

TEST_F(TimePeriodTest, isBussyShouldReturnFalseAtBegin)
{
    ASSERT_FALSE(_sut.isBusy());
}

TEST_F(TimePeriodTest, isBussyShouldReturnTrueAfterMarkingTimePeriodAsBussy)
{
    _sut.makeBusy();

    ASSERT_TRUE(_sut.isBusy());
}

TEST_F(TimePeriodTest, isBussyShouldReturnFalseAfterMarkingTimePeriodAsFree)
{
    _sut.makeFree();

    ASSERT_TRUE(_sut.makeFree());
}
