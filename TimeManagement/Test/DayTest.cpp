#include <gtest/gtest.h>
#include "Day.hpp"

using namespace ::testing;

namespace
{
    static constexpr auto twentyFourHours = 288u;
    static constexpr auto hour = 12u;
    static constexpr auto quarterOfAnHour = 3u;
    static constexpr auto fiveMinutes = 1u;
}

TEST(DayTest, DayIsConstructable)
{
    Day();
}

TEST(DayTest_getFirstSlot, DefaultCtorShouldCreateOneFreeSlot)
{
    std::pair<unsigned int, unsigned int> holeDayFreesSlot = std::make_pair(0, twentyFourHours);

    Day day;
    ASSERT_EQ(day.getFirstFreeSlot(), holeDayFreesSlot);
}

TEST(DayTest_getFirstSlot, DefaultCtorShouldHaveSlorForTwentyFourHours)
{
    std::pair<unsigned int, unsigned int> holeDayFreesSlot = std::make_pair(0, twentyFourHours);

    Day day;
    ASSERT_EQ(day.getFirstFreeSlot(twentyFourHours), holeDayFreesSlot);
}

TEST(DayTest_getFirstSlot, DefaultCtorShouldHaveSlotSmallerThanTwentyFourHours)
{
    std::pair<unsigned int, unsigned int> holeDayFreesSlot = std::make_pair(0, twentyFourHours);

    Day day;
    ASSERT_EQ(day.getFirstFreeSlot(2*hour + quarterOfAnHour), holeDayFreesSlot);
}

TEST(DayTest_getFirstSlot, ThereShouldNotBeAnySlotBiggerThanHoleDay)
{
    Day day;
    ASSERT_EQ(day.getFirstFreeSlot(twentyFourHours + hour), std::nullopt);
}

TEST(DayTest_reserveSlot, ReserveDayShouldReturnTrueForDefaultConstructor)
{
    Day day;
    ASSERT_TRUE(day.reserveSlot(0, twentyFourHours));
}

TEST(DayTest_reserveSlot, ReserveSlotSmalletThanDayShouldReturnTrueForDefaultConstructor)
{
    Day day;
    ASSERT_TRUE(day.reserveSlot(0, 2*hour + quarterOfAnHour));
}

TEST(DayTest_reserveSlot, ReserveMoreThanDayShouldReturnFalse)
{
    Day day;
    ASSERT_FALSE(day.reserveSlot(0, twentyFourHours + hour));
}

TEST(DayTest_reserveSlot, AfterReservationHoleDayThereShouldNotBeAnyFreeSlot)
{
    Day day;
    day.reserveSlot(0, twentyFourHours);

    ASSERT_EQ(day.getFirstFreeSlot(), std::nullopt);
}

TEST(DayTest_reserveSlot, AfterReservationToMiddayThereShouldBeStillFreeSlot)
{
    std::pair<unsigned int, unsigned int> halfDayFreesSlot = std::make_pair(12*hour, 12*hour);

    Day day;
    day.reserveSlot(0, 12*hour);

    ASSERT_EQ(day.getFirstFreeSlot(), halfDayFreesSlot);
}

TEST(DayTest_reserveSlot, AfterReservationFromMiddayThereShouldBeFreeSlotBeforeMidday)
{
    std::pair<unsigned int, unsigned int> halfDayFreesSlot = std::make_pair(0, 12*hour);

    Day day;
    day.reserveSlot(12*hour, 12*hour);

    ASSERT_EQ(day.getFirstFreeSlot(), halfDayFreesSlot);
}

TEST(DayTest_reserveSlot, AfterReservationTwentyHoursFromOnePmThereShouldBeOneHourSlotAndThreeHourSlot)
{
    std::pair<unsigned int, unsigned int> oneHourFreeSlot = std::make_pair(0, 1*hour);
    std::pair<unsigned int, unsigned int> threeHourFreeSlot = std::make_pair(21*hour, 3*hour);

    Day day;
    day.reserveSlot(1*hour, 20*hour);

    ASSERT_EQ(day.getFirstFreeSlot(1*hour), oneHourFreeSlot);
    ASSERT_EQ(day.getFirstFreeSlot(3*hour), threeHourFreeSlot);
}

TEST(DayTest_reserveSlot, AfterReservationThreeEightHoursSlotsThereShouldNotBeAnyFreeSlot)
{
    Day day;
    day.reserveSlot(0, 8*hour);
    day.reserveSlot(16*hour, 8*hour);
    day.reserveSlot(8*hour, 8*hour);

    ASSERT_FALSE(day.getFirstFreeSlot());
    ASSERT_FALSE(day.getFirstFreeSlot());
}

TEST(DayTest, DayIsConstructableByInitializerLists)
{
    Day day {{0, 4*hour}, {20*hour, 2*hour}};
}

TEST(DayTest, DayConstructedByInitializerListWithFourParametersShouldHaveFourFreeSlots)
{
    std::pair<unsigned int, unsigned int> firstFreeSlot = std::make_pair(0, 2*hour);
    std::pair<unsigned int, unsigned int> secondFreeSlot = std::make_pair(3*hour, 4*hour);
    std::pair<unsigned int, unsigned int> thirdFreeSlot = std::make_pair(8*hour, 6*hour);
    std::pair<unsigned int, unsigned int> fourthFreeSlot = std::make_pair(15*hour, 8*hour);

    Day day {{0, 2*hour}, {3*hour, 4*hour}, {8*hour, 6*hour}, {15*hour, 8*hour}};

    ASSERT_EQ(day.getFirstFreeSlot(1*hour), firstFreeSlot);
    ASSERT_EQ(day.getFirstFreeSlot(4*hour), secondFreeSlot);
    ASSERT_EQ(day.getFirstFreeSlot(5*hour), thirdFreeSlot);
    ASSERT_EQ(day.getFirstFreeSlot(8*hour), fourthFreeSlot);

}
