#include "Day.hpp"
#include <iostream>

/**
 * 288u to cała doba
 * TimePeriod tak naprawdę jest nam niepotrzebny
 * Powinien on tylko w czasie kompilacji wyliczać to 288u
 * Oraz udostępniać namespace, podobny do tego z DayTest.cpp
 */
Day::Day()
{
    _capability.emplace_front(0, 288u);
}

Day::Day(std::initializer_list<std::pair<unsigned int, unsigned int>> freeSlots)
        : _capability(freeSlots)
{
    // NOOP
}

DailyCapability Day::getDailyCapability()
{
    return _capability;
}

bool Day::reserveSlot(unsigned int begin, unsigned int numberOfNeededPeriod)
{
    auto iterator = _capability.begin();

    if(numberOfNeededPeriod > 288u)
    {
        return false;
    }

    while(_capability.end() != iterator)
    {
        auto currentTime = std::get<PairValue::hour>(*iterator);
        auto currentNumberOfPeriod = std::get<PairValue::numberOfPeriods>(*iterator);
        if(currentTime == begin && numberOfNeededPeriod <= std::get<PairValue::numberOfPeriods>(*iterator))
        {
            if(numberOfNeededPeriod == std::get<PairValue::numberOfPeriods>(*iterator))
            {
                _capability.erase(iterator);
            }
            else if(numberOfNeededPeriod < std::get<PairValue::numberOfPeriods>(*iterator))
            {
                auto beginOfNewSlot = begin + numberOfNeededPeriod;
                auto numberOfPeriodInNewSlot = currentNumberOfPeriod - numberOfNeededPeriod;
                auto newSlotAfterReservation = std::make_pair(beginOfNewSlot, numberOfPeriodInNewSlot);
                _capability.insert(iterator, newSlotAfterReservation);
                _capability.erase(iterator);
            }
            else
            {
                return false;
            }
            return true;
        }
        else if(currentTime + currentNumberOfPeriod > begin)
        {
            auto newSlotBeforeReservation = std::make_pair(currentTime, begin - currentTime);
            _capability.insert(iterator, newSlotBeforeReservation);
            if(currentNumberOfPeriod - numberOfNeededPeriod - (begin - currentTime) != 0)
            {
                auto beginOfNewSlotAfterReservation = begin + numberOfNeededPeriod;
                auto numberOfPeriodInNewSlotAfterReservation = currentNumberOfPeriod - numberOfNeededPeriod - begin - currentTime;
                auto newSlotAfterReservation = std::make_pair(beginOfNewSlotAfterReservation, numberOfPeriodInNewSlotAfterReservation);
                _capability.insert(iterator, newSlotAfterReservation);
            }
            _capability.erase(iterator);
            return true;
        }
        ++iterator;
    }
    return false;
}

std::optional<std::pair<unsigned int, unsigned int>> Day::getFirstFreeSlot(unsigned int numberOfNeededPeriods)
{
    for(const auto& iterator : _capability)
    {
        if(numberOfNeededPeriods <= std::get<PairValue::numberOfPeriods>(iterator))
        {
            return std::make_pair(std::get<PairValue::hour>(iterator), std::get<PairValue::numberOfPeriods>(iterator));
        }
    }
    return std::nullopt;
}
