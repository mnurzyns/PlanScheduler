#include "TimePeriod.hpp"


void TimePeriod::makeBusy()
{
    _busy = true;
}

bool TimePeriod::isBusy()
{
    return _busy;
}

bool TimePeriod::makeFree()
{
    return true;
}