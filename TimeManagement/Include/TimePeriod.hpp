class TimePeriod
{
public:
    TimePeriod() = default;
    int time;

    void makeBusy();
    bool isBusy();
    bool makeFree();

private:
    bool _busy = false;
};
