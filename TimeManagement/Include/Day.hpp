#pragma once

#include <list>
#include <optional>

using DailyCapability = std::list<std::pair<unsigned int, unsigned int>>;

namespace PairValue
{
static constexpr auto hour = 0;
static constexpr auto numberOfPeriods = 1;
}

class Day
{
public:
    Day();
    Day(std::initializer_list<std::pair<unsigned int, unsigned int>> freeSlots);
    DailyCapability getDailyCapability();
    bool reserveSlot(unsigned int begin, unsigned int numberOfNeededPeriod);
    std::optional<std::pair<unsigned int, unsigned int>> getFirstFreeSlot(unsigned int numberOfNeededPeriods=0);

private:
    DailyCapability _capability;
};
